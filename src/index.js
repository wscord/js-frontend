import m from "mithril"

//document.body = document.createElement("body");

m.render(document.body, [
	m("div", {
		class: "headerbar",
	}, "Todo"),
	m("div", {
		class: "messages",
		id: "message-container",
	}, "Messages"),
])
